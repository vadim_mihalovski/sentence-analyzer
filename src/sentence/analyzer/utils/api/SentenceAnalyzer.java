package sentence.analyzer.utils.api;

import java.util.Arrays;
import java.util.Map;

public interface SentenceAnalyzer {

    static int hashWithLeftOffset(String word) {
        int result = 100;
        Character[] letters = word.chars().mapToObj(ch -> (char) ch).toArray(Character[]::new);
        Arrays.sort(letters, (a, b) -> (Character.toLowerCase(a) - Character.toLowerCase(b)));
        for (int i=0; i<letters.length; i++) {
            result += (i+1)*((Character.toLowerCase(letters[i]) + Character.toUpperCase(letters[i])) << 2);
        }
        return result;
    }

    boolean analyzeSentenceWithSpaces(String sentence, Map<Integer, String> dictionary);

    boolean analyzeSentenceWithoutSpaces(String sentence, Map<Integer, String> dictionary);
}
