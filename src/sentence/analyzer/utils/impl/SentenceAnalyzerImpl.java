package sentence.analyzer.utils.impl;

import sentence.analyzer.utils.api.SentenceAnalyzer;

import java.util.Map;

public class SentenceAnalyzerImpl implements SentenceAnalyzer {

    private static final String SPACE = " ";

    @Override
    public boolean analyzeSentenceWithSpaces(String sentence, Map<Integer, String> dictionary) {
        if (sentence != null && !sentence.isEmpty()) {
            String[] words = sentence.split(SPACE);
            for (String word : words) {
                if (!dictionary.containsKey(SentenceAnalyzer.hashWithLeftOffset(word))) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean analyzeSentenceWithoutSpaces(String sentence, Map<Integer, String> dictionary) {
        if(sentence == null || sentence.isEmpty()){
            return false;
        }else{
            for(int i=1;i<=sentence.length();i++){
                String currentWord=sentence.substring(0,i);
                String restOfTheLine=sentence.substring(i);
                int currentWordHash = SentenceAnalyzer.hashWithLeftOffset(currentWord);
                if(dictionary.containsKey(currentWordHash) && !restOfTheLine.isEmpty()){
                    boolean result = analyzeSentenceWithoutSpaces(restOfTheLine, dictionary);
                    if (result) {
                        return true;
                    }
                } else if (dictionary.containsKey(currentWordHash) && restOfTheLine.isEmpty()) {
                    return true;
                }
            }
        }
        return false;
    }
}
