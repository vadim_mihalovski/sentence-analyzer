package sentence.analyzer;

import sentence.analyzer.utils.api.SentenceAnalyzer;
import sentence.analyzer.utils.impl.SentenceAnalyzerImpl;

import java.util.HashMap;
import java.util.Map;

public class EntryPoint {

    private static final String RESULT_DELIMITER = ": ";
    private static Map<Integer, String> dictionary;

    static {
        dictionary = new HashMap<>();
        dictionary.put(SentenceAnalyzer.hashWithLeftOffset("hello"), "hello");
        dictionary.put(SentenceAnalyzer.hashWithLeftOffset("he"), "he");
        dictionary.put(SentenceAnalyzer.hashWithLeftOffset("to"), "to");
        dictionary.put(SentenceAnalyzer.hashWithLeftOffset("the"), "the");
        dictionary.put(SentenceAnalyzer.hashWithLeftOffset("world"), "world");
        dictionary.put(SentenceAnalyzer.hashWithLeftOffset("I"), "I");
        dictionary.put(SentenceAnalyzer.hashWithLeftOffset("have"), "have");
        dictionary.put(SentenceAnalyzer.hashWithLeftOffset("a"), "a");
        dictionary.put(SentenceAnalyzer.hashWithLeftOffset("car"), "car");
        dictionary.put(SentenceAnalyzer.hashWithLeftOffset("never"), "never");
        dictionary.put(SentenceAnalyzer.hashWithLeftOffset("ever"), "ever");
        dictionary.put(SentenceAnalyzer.hashWithLeftOffset("been"), "been");
        dictionary.put(SentenceAnalyzer.hashWithLeftOffset("an"), "an");
        dictionary.put(SentenceAnalyzer.hashWithLeftOffset("apple"), "apple");
    }

    public static void main(String[] args) {
        SentenceAnalyzer sentenceAnalyzer = new SentenceAnalyzerImpl();

        System.out.println("Test sentences with spaces: ");
        String sentence1 = "I have a car";
        String sentence2 = "I aveh a rac";
        String sentence3 = "I have an apple";
        String sentence4 = "I haev na pleap";
        String sentence5 = "I haev na pleapd";
        String sentence6 = "Ih aev n apleap";
        System.out.println(sentence1 + RESULT_DELIMITER + sentenceAnalyzer.analyzeSentenceWithSpaces(sentence1, dictionary));
        System.out.println(sentence2 + RESULT_DELIMITER + sentenceAnalyzer.analyzeSentenceWithSpaces(sentence2, dictionary));
        System.out.println(sentence3 + RESULT_DELIMITER + sentenceAnalyzer.analyzeSentenceWithSpaces(sentence3, dictionary));
        System.out.println(sentence4 + RESULT_DELIMITER + sentenceAnalyzer.analyzeSentenceWithSpaces(sentence4, dictionary));
        System.out.println(sentence5 + RESULT_DELIMITER + sentenceAnalyzer.analyzeSentenceWithSpaces(sentence5, dictionary));
        System.out.println(sentence6 + RESULT_DELIMITER + sentenceAnalyzer.analyzeSentenceWithSpaces(sentence6, dictionary));

        System.out.println("Test sentences without spaces: ");
        String sentence7 = "Ihaveacar";
        String sentence8 = "Ihaevarac";
        String sentence9 = "Ihaveneverbeen";
        String sentence10 = "Ihaveeverbeen";
        String sentence11 = "Ihaveevernbeen";
        String sentence12 = "Ihavenaapple";
        String sentence13 = "Ihavednaapple";
        String sentence14 = "Ihavenaappler";
        System.out.println(sentence7 + RESULT_DELIMITER + sentenceAnalyzer.analyzeSentenceWithoutSpaces(sentence7, dictionary));
        System.out.println(sentence8 + RESULT_DELIMITER + sentenceAnalyzer.analyzeSentenceWithoutSpaces(sentence8, dictionary));
        System.out.println(sentence9 + RESULT_DELIMITER + sentenceAnalyzer.analyzeSentenceWithoutSpaces(sentence9, dictionary));
        System.out.println(sentence10 + RESULT_DELIMITER + sentenceAnalyzer.analyzeSentenceWithoutSpaces(sentence10, dictionary));
        System.out.println(sentence11 + RESULT_DELIMITER + sentenceAnalyzer.analyzeSentenceWithoutSpaces(sentence11, dictionary));
        System.out.println(sentence12 + RESULT_DELIMITER + sentenceAnalyzer.analyzeSentenceWithoutSpaces(sentence12, dictionary));
        System.out.println(sentence13 + RESULT_DELIMITER + sentenceAnalyzer.analyzeSentenceWithoutSpaces(sentence13, dictionary));
        System.out.println(sentence14 + RESULT_DELIMITER + sentenceAnalyzer.analyzeSentenceWithoutSpaces(sentence14, dictionary));
    }
}
